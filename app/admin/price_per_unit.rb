# ActiveAdmin.register PricePerUnit do
#   actions :all
#   menu parent: 'Artworks', label: "Price per Unit"
#   index do
#     selectable_column
#     column :id
#     column :artwork
#     column :number_per_unit
#     column :price
#     column :effectivity_date
#     actions
#   end
#   form do |f|
#     @artwork = controller.instance_variable_get(:@artwork)
#     @artwork ||= Artwork.all
#     f.semantic_errors *f.object.errors.keys
#     f.inputs do
#       f.input :artwork, input_html: { class: "chosen-input", style: "width:75% !important"},
#         collection: @artwork, prompt: false, include_blank: false
#       f.input :number_per_unit, label: "Per Inch"
#       f.input :price
#       f.input :effectivity_date, as: :date_picker, input_html:
#         {style: "width:75% !important"}
#     end
#     f.actions
#   end
#   controller do
#     def new
#       @artwork = Artwork.where id: params[:artwork_id]
#       super
#     end
#     def create
#       super do |success,failure|
#         success.html { redirect_to admin_artwork_path(resource.artwork) }
#       end
#     end
#     def update
#       super do |success,failure|
#         success.html { redirect_to admin_artwork_path(resource.artwork) }
#       end
#     end
#   end
#   permit_params :id, :number_per_unit, :price, :effectivity_date, :artwork_id
# end