ActiveAdmin.register ArtworkStyle do
  menu if: proc{ current_admin_user.admin? }
  menu parent: 'Artworks', label: "Styles"
  actions :all
  filter :name
  index do
    selectable_column
    column :id
    column :name
    column :description
    actions
  end
  show do
    panel(I18n.t('active_admin.details', model: active_admin_config.resource_label)) do
      attributes_table_for resource do
        row :id
        row :name
        row :description
      end
    end
  end
  permit_params :id, :description, :name
  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs do
      f.input :name
      f.input :description, as: :text
    end
    f.actions
  end
end