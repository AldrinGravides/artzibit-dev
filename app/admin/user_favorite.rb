ActiveAdmin.register UserFavorite do
  menu if: proc{ current_admin_user.admin? }
  menu parent: 'Users', label: "Favorites"
  actions :all, except: :show
  filter :name
  index do
    selectable_column
    column :user
    column :artwork
    column :deleted
    actions
  end
  permit_params :id, :user_id, :artwork_id, :deleted
  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs do
      f.input :user, input_html: { class: "chosen-input", style: "width:75% !important" }, collection: User.all.map{|u|[u.name, u.id]}, include_blank: false
      f.input :artwork, input_html:   { class: "chosen-input", style: "width:75% !important" }, collection: Artwork.all.map{|u|[u.name, u.id]}, include_blank: false
      f.input :deleted
    end
    f.actions
  end
  controller do
    def create
      super do |success,failure|
        success.html { redirect_to admin_user_favorites_path }
      end
    end
    def update
      super do |success,failure|
        success.html { redirect_to admin_user_favorites_path }
      end
    end
  end
end