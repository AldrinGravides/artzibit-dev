ActiveAdmin.register InstallationRange do
  menu if: proc{ current_admin_user.admin? }
  index do
    selectable_column
    column :id
    column :min_size
    column :max_size
    column "Current Price" do |x|
      x.current_price
    end
    column "Actions" do |row|
      (link_to "Add Price", new_admin_installation_price_path(fixed_size_id: row),
        method: :get)
    end
    actions
  end
  action_item only: :show do
    (link_to "Add Price", new_admin_installation_price_path(installation_range_id: resource),
        method: :get)
  end
  show do
    panel(I18n.t('active_admin.details', model: active_admin_config.resource_label)) do
      attributes_table_for resource do
        row :id
        row "Minimum Size" do
          resource.min_size
        end
        row "Maximum Size" do
          resource.max_size
        end
      end
    end
    panel "Price History" do
      table_for resource.installation_prices.order(:effectivity_date) do
        column :price
        column :effectivity_date
      end
    end
  end
  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs do
      f.input :min_size
      f.input :max_size
    end
    f.actions
  end
  permit_params :id, :min_size, :max_size
end