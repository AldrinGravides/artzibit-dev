# ActiveAdmin.register SizeOption do
#   menu parent: 'Artworks'
#   actions :all
#   filter :name
#   index do
#     selectable_column
#     column :id
#     column :name
#     actions
#   end
#   show do
#     panel(I18n.t('active_admin.details', model: active_admin_config.resource_label)) do
#       attributes_table_for resource do
#         row :id
#         row :name
#       end
#     end
#   end
#   permit_params :id, :description, :name
#   form do |f|
#     f.semantic_errors *f.object.errors.keys
#     f.inputs do
#       f.input :name
#     end
#     f.actions
#   end
# end