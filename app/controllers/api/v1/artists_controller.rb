class Api::V1::ArtistsController < UserModuleController
  respond_to :json
  def index
    @artists = User.all.map{|c| c.v1_format }
    respond_with status: true, data: @artists
  end
end