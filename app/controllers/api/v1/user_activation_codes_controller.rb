class Api::V1::UserActivationCodesController < ApplicationController
  respond_to :json
  def create
    @activation_code = ActivationCode.find_by user_activation_code_param
    unless @activation_code.blank?

      if @activation_code.reusable
        @user_activation_code = UserActivationCode.new
        @user_activation_code.activation_code_id = @activation_code.id
        if @user_activation_code.save
          render json: {status: true, data: @user_activation_code}
        else
          render json: {status: false, message: @user_activation_code.errors.full_messages.first}
        end
      else
        if @activation_code.limit.to_i > @activation_code.reserved_and_used_count
          @user_activation_code = UserActivationCode.new
          @user_activation_code.activation_code_id = @activation_code.id
          if @user_activation_code.save
            render json: {status: true, data: @user_activation_code}
          else
            render json: {status: false, message: @user_activation_code.errors.full_messages.first}
          end
        else
          render json: {status: false, message: "Activation Code reaches its limit used"}
        end
      end
    else
      render json: {status: false, message: "Activation Code is invalid."}
    end
  end
  private
  def user_activation_code_param
    params.required(:user_activation_code).permit(:code)
  end
end