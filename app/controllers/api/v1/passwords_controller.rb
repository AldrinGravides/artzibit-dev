class Api::V1::PasswordsController < Users::PasswordsController
  respond_to :json
  def create
    user = User.where params[:user]
    unless user.blank?
      if user.first.log_thru_facebook?
        render json: {message: "Your user not log thru facebook", status: false}
      else
        super
      end
    else
      render json: {message: "User not found", status: false}
    end
  end
end