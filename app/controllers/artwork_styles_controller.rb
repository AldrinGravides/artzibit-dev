class ArtworkStylesController < ApplicationController
  respond_to :json
  def index
    @artwork_styles = ArtworkStyle.all.order(:name)
    respond_to do |format|
      format.json {render json: @artwork_styles}
    end
  end
end