class PageController < ApplicationController
  def home
    @test_user = TestUser.new
    @user = User.new

    # redirect to artworks controller
    unless current_user.blank?
      redirect_to artworks_path
    end
  end
end
