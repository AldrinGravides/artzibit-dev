class Category < ActiveRecord::Base
  attr_accessor :remove_image
  has_attached_file :cover_image
  has_many :artworks
  validates_attachment_content_type :cover_image,
    content_type: /^image\/(jpg|jpeg|pjpeg|png|x-png|gif)$/, unless: "cover_image.blank?"
  validates :description, :name, presence: true
  validates :name, uniqueness: true
  before_save :remove_photo, if: "remove_image == '1'"
  def v1_format
    {
      id: id,
      name: name,
      description: description,
      image: cover_image
    }
  end
  private
  def remove_photo
    self.cover_image.destroy
    true
  end
end