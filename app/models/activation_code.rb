class ActivationCode < ActiveRecord::Base
  validates :code, presence: true, uniqueness: true
  validates :limit, presence: true, unless: "reusable == true"

  has_many :user_activation_codes, dependent: :delete_all
  has_many :users, through: :user_activation_codes

  def reserved_and_used_count
    user_activation_codes.where("? <= expiration or user_id IS NOT NULL",
      Time.now).count
  end
end
