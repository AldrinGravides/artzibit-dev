class DeliveryRange < ActiveRecord::Base
  validates :min_size, :max_size, presence: true
  has_many :delivery_prices
  def name
    "#{min_size} - #{max_size} #{'(with Installation)' if with_installation?}"
  end
  def current_delivery_price
    delivery_prices.where("effectivity_date <= ?",Time.now).order("effectivity_date desc").first
  end
  def current_price
    current_delivery_price.try(:price)
  end
end
