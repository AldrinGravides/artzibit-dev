class BasePrice < ActiveRecord::Base
  validates :percentage, :effectivity_date, presence: true

  def name
    "#{percentage}%"
  end
  def self.current_price
    BasePrice.where("effectivity_date <= ?",Time.now).order("effectivity_date desc").first
  end
end
