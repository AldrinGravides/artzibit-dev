class InstallationRange < ActiveRecord::Base
  has_many :installation_prices
  validates :min_size, :max_size, presence: true

  def name
    "#{min_size} - #{max_size}"
  end
  def current_range_price
    installation_prices.where("effectivity_date <= ?", Time.now).order("effectivity_date desc").first
  end
  def current_price
    current_range_price.try(:price)
  end
end
