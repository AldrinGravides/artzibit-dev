class ArtworkPrice < ActiveRecord::Base
  belongs_to :artwork
  belongs_to :artwork_size
  validates :artwork_id, :artwork_size_id, :effectivity_date, :price,
    presence: true
  before_save :price_not_zero
  def price_not_zero
    errors.add(:base, "Price shoudl be > 0") if price == 0
  end
  def name
    "#{price} - (Effectivity Date: #{effectivity_date.strftime('%b %d, %Y')})"
  end
end
