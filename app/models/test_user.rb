class TestUser < ActiveRecord::Base
  validates :email, presence: true
  validates :email, uniqueness: true
  # validates_email_realness_of :email

  validate :validate_email

  private
  def validate_email
    begin
      valid = EmailVerifier.check(self.email)
    rescue
      errors.add(:base, "Please enter a valid email")
    else
      errors.add(:base, "Please enter a valid email") unless valid == true
    end
  end
end
