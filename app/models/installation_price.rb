class InstallationPrice < ActiveRecord::Base
  validates :price, :effectivity_date, presence: true
  belongs_to :installation_range
  def self.current_price
    ins_pri = InstallationPrice.where("effectivity_date <= ?", Time.now).order(:effectivity_date).last
    ins_pri
  end
  def name
    "$#{price}: Artwork size between (#{installation_range.min_size}cm - #{installation_range.max_size}cm)"
  end
end
