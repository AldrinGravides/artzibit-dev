class PricePerUnit < ActiveRecord::Base
  belongs_to :artwork
  validates :number_per_unit, :price, :effectivity_date, :artwork_id,
    presence: true
  def v1_format
    {
      id: id,
      unit: number_per_unit,
      price: price
    }
  end
  def name
    "$ #{price}/#{number_per_unit} inch for excess"
  end
end