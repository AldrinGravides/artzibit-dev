class ArtworkSize < ActiveRecord::Base
  belongs_to :artwork
  has_many :artwork_prices, dependent: :destroy
  validates :height, :width, presence: true
  validate :validate_number_of_sizes
  validate :validate_featured
  def validate_number_of_sizes
    if ["Fixed","Scalable"].include? artwork.size_type
      if persisted?
        errors.add(:base, "Only 1 size for #{artwork.size_type}") if artwork.artwork_sizes.size > 1
      else
        errors.add(:base, "Only 1 size for #{artwork.size_type}") if artwork.artwork_sizes.count == 1
      end
    end
  end
  def format_size
    "#{height} X #{width}"
  end
  def name
    if artwork.scalable?
      "Min: #{format_size}"
    else
      format_size
    end
  end
  def validate_featured
    errors.add(:featured, "Only Presets can be featured") if !artwork.presets? and featured == true
    errors.add(:featured, "Only 1 can be featured") unless artwork.artwork_sizes.where("featured = ? and id != ?", true, id).blank?
  end
  def latest_price
    artwork_prices.where("effectivity_date <= ?", Time.now).
      order("effectivity_date desc, id desc").first
  end
  def featured_price
    latest_price.try(:price)
  end
  def v1_format
    {
      size_id: id,
      height: height,
      width: width,
      price_id: latest_price.id,
      price: featured_price
    }
  end
end
