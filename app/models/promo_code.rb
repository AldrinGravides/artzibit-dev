class PromoCode < ActiveRecord::Base
  validates :code, :start_date, presence: true
  validates :percentage_off, presence: true, if: "price_off.blank?"
  validates :price_off, presence: true, if: "percentage_off.blank?"

  has_many :carts, dependent: :destroy
  has_many :promo_code_dtls, dependent: :destroy

  accepts_nested_attributes_for :promo_code_dtls, allow_destroy: true
end
