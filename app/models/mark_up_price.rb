class MarkUpPrice < ActiveRecord::Base
  belongs_to :artwork

  before_create :add_effectivity_date

  def add_effectivity_date
    self.effectivity_date = Time.now
  end
end
