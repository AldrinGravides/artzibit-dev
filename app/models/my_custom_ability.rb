class MyCustomAbility
  include CanCan::Ability
  def initialize(admin_user)
    can :read, ActiveAdmin::Page, :name => "Dashboard"
    if admin_user.admin?
      can :manage, :all
    elsif admin_user.can_print?
      can :manage, Order
      can :manage, OrderHistory
      can :manage, OrderAssignment
    elsif admin_user.can_install?
      can :manage, Order
      can :manage, OrderHistory
      can :manage, OrderAssignment
    elsif admin_user.vetter?
      can :manage, ArtworkApprovalDtl
    end
  end
end