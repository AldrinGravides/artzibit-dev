class UserAppTrackingLog < ActiveRecord::Base
  belongs_to :user
  belongs_to :artwork
  validates :user_id, :start_time, :end_time, presence: true

  scope :app, -> {where(artwork_id: nil)}
  scope :artwork, -> {where("artwork_id IS NOT NULL")}

  before_save :compute_duration
  def compute_duration
    self.duration = end_time - start_time
  end
end
