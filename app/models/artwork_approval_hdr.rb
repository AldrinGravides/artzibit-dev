class ArtworkApprovalHdr < ActiveRecord::Base
  belongs_to :artwork
  has_many :artwork_approval_dtls, dependent: :destroy

  accepts_nested_attributes_for :artwork_approval_dtls, allow_destroy: true

  after_create :create_details

  def create_details
    admin_users = AdminUser.where(vetter: true)
    admin_users.each do |am|
      approval_dtl = ArtworkApprovalDtl.new
      approval_dtl.artwork_approval_hdr_id = self.id
      approval_dtl.admin_user_id = am.id
      approval_dtl.status = "pending"
      approval_dtl.save
    end
  end

  validates :artwork, presence: true

  def status
    no_of_approval = NoOfApproval.where("effectivity_date <= ?", Time.now).order(:effectivity_date).last.try(:count)
    no_of_approval ||= 2
    if artwork_approval_dtls.approves.count >= no_of_approval
      "approve"
    else
      if artwork_approval_dtls.pendings.count >= (no_of_approval - artwork_approval_dtls.approves.count) and expiration_date.to_i >= Time.now.to_i
        "pending"
      else
        "reject"
      end
    end
  end
end
