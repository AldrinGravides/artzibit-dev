class Event < ActiveRecord::Base
  validates :title, presence: true
  validates :title, uniqueness: true
  has_many :event_batches, dependent: :destroy

  has_many :event_artworks
  has_many :artworks, through: :event_artworks

  accepts_nested_attributes_for :event_batches, allow_destroy: true
  accepts_nested_attributes_for :event_artworks, allow_destroy: true

  def featured_image
    artworks.actives.first.featured_image
  end
end
