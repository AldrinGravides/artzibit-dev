# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
AdminUser.create(email: 'admin@example.com', password: 'password', password_confirmation: 'password', admin: true) if AdminUser.where(email: "admin@example.com").blank?

# Create Category
Category.create(name: "Fixed", description: "Unique artworks in their original sizes") if Category.where(name: "Fixed").blank?
Category.create(name: "Custom", description: "Your favorite pieces in all size imaginable") if Category.where(name: "Custom").blank?