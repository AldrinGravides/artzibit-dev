class CreateArtworkApprovalHdrs < ActiveRecord::Migration
  def change
    create_table :artwork_approval_hdrs do |t|
      t.references :artwork, index: true

      t.timestamps null: false
    end
    add_foreign_key :artwork_approval_hdrs, :artworks
  end
end
