class AddActiveToArtworks < ActiveRecord::Migration
  def change
    add_column :artworks, :active, :boolean, default: true
  end
end
