class CreateNoOfApprovals < ActiveRecord::Migration
  def change
    create_table :no_of_approvals do |t|
      t.integer :count
      t.datetime :effectivity_date

      t.timestamps null: false
    end
  end
end
