class CreateUserActivationCodes < ActiveRecord::Migration
  def change
    create_table :user_activation_codes do |t|
      t.references :user, index: true
      t.references :activation_code, index: true

      t.timestamps null: false
    end
    add_foreign_key :user_activation_codes, :users
    add_foreign_key :user_activation_codes, :activation_codes
  end
end
