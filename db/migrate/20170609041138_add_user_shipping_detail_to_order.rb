class AddUserShippingDetailToOrder < ActiveRecord::Migration
  def change
    add_column :orders, :user_shipping_detail_id, :integer
  end
end
