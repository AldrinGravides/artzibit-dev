class CreateFixedSizes < ActiveRecord::Migration
  def change
    create_table :fixed_sizes do |t|
      t.string :label
      t.string :size_type
      t.decimal :height
      t.decimal :width

      t.timestamps null: false
    end
  end
end
