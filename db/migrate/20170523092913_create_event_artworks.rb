class CreateEventArtworks < ActiveRecord::Migration
  def change
    create_table :event_artworks do |t|
      t.references :event, index: true
      t.references :artwork, index: true

      t.timestamps null: false
    end
    add_foreign_key :event_artworks, :events
    add_foreign_key :event_artworks, :artworks
  end
end
