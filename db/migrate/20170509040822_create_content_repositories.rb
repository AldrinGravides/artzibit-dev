class CreateContentRepositories < ActiveRecord::Migration
  def change
    create_table :content_repositories do |t|
      t.string :title
      t.string :subtitle
      t.text :description
      t.integer :sequence_no
      t.text :html_content
      t.string :redirect_to

      t.timestamps
    end
  end
end
