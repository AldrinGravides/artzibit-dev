class AddAttachmentCoverImageToCategories < ActiveRecord::Migration
  def self.up
    change_table :categories do |t|
      t.attachment :cover_image
    end
  end

  def self.down
    remove_attachment :categories, :cover_image
  end
end
