class AddPrivacyToArtwork < ActiveRecord::Migration
  def change
    add_column :artworks, :privacy, :string
  end
end
