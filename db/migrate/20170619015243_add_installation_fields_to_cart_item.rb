class AddInstallationFieldsToCartItem < ActiveRecord::Migration
  def change
    add_column :cart_items, :installation, :boolean
    add_column :cart_items, :installation_price_id, :integer
  end
end
