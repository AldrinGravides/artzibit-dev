class AddAttachmentImageToArtworkImages < ActiveRecord::Migration
  def self.up
    change_table :artwork_images do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :artwork_images, :image
  end
end
