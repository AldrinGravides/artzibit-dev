class AddCropFieldsToArtworkImages < ActiveRecord::Migration
  def change
    add_column :artwork_images, :crop_x, :integer
    add_column :artwork_images, :crop_y, :integer
    add_column :artwork_images, :crop_w, :integer
    add_column :artwork_images, :crop_h, :integer
  end
end
