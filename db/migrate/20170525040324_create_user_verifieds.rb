class CreateUserVerifieds < ActiveRecord::Migration
  def change
    create_table :user_verifieds do |t|
      t.references :user, index: true
      t.boolean :deleted
      t.datetime :effectivity_date

      t.timestamps null: false
    end
    add_foreign_key :user_verifieds, :users
  end
end
