class AddCustomerStripeIdToUser < ActiveRecord::Migration
  def change
    add_column :users, :customer_stripe_id, :string
  end
end
