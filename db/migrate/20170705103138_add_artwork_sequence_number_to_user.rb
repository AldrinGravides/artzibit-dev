class AddArtworkSequenceNumberToUser < ActiveRecord::Migration
  def change
    add_column :users, :artwork_sequence_number, :integer
  end
end
