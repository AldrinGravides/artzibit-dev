class CreateArtworkStyles < ActiveRecord::Migration
  def change
    create_table :artwork_styles do |t|
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end
